import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { getWarehouses, loadWarehouses } from 'ish-core/store/core/warehouse';

// tslint:disable:member-ordering
@Injectable({ providedIn: 'root' })
export class WarehouseFacade {
  constructor(private store: Store) {}

  warehouses$ = this.store.pipe(select(getWarehouses));

  getWarehouses$() {
    this.store.dispatch(loadWarehouses());
  }
}
