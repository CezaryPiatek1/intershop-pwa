import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { anything, capture, spy, verify } from 'ts-mockito';

import { WarehouseFacade } from 'ish-core/facades/warehouse.facade';

describe('Warehouse Facade', () => {
  let store$: MockStore;
  let storeSpy$: MockStore;
  let facade: WarehouseFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore()],
    });
    store$ = TestBed.inject(MockStore);
    facade = TestBed.inject(WarehouseFacade);
    storeSpy$ = spy(store$);
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  describe('getWarehouses$()', () => {
    it('should dispatch Warehouse action on store', () => {
      facade.getWarehouses$();
      verify(storeSpy$.dispatch(anything())).once();
      expect(capture(storeSpy$.dispatch).last()).toMatchInlineSnapshot(`[Warehouse API] Load Warehouses`);
    });
  });
});
