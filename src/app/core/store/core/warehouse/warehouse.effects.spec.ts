import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { cold, hot } from 'jest-marbles';
import { Observable, of } from 'rxjs';
import { instance, mock, verify, when } from 'ts-mockito';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';
import { ConfigurationService } from 'ish-core/services/configuration/configuration.service';
import { WarehouseService } from 'ish-core/services/warehouse/warehouse.service';
import { CoreStoreModule } from 'ish-core/store/core/core-store.module';
import { loadWarehouses, loadWarehousesSuccess } from 'ish-core/store/core/warehouse/warehouse.actions';
import { CustomerStoreModule } from 'ish-core/store/customer/customer-store.module';

import { WarehouseEffects } from './warehouse.effects';

describe('Warehouse Effects', () => {
  let actions$: Observable<Action>;
  let warehouseServiceMock: WarehouseService;
  let effects: WarehouseEffects;
  let store$: Store;

  const mockedWarehouse = { country: 'Japan', city: 'Tokyo', houseNumber: 3212 } as Warehouse;

  beforeEach(() => {
    const configurationServiceMock = mock(ConfigurationService);
    when(configurationServiceMock.getServerConfiguration()).thenReturn(of({}));

    TestBed.configureTestingModule({
      imports: [
        CoreStoreModule.forTesting(['configuration', 'serverConfig', 'warehouse']),
        CustomerStoreModule.forTesting('user'),
        HttpClientTestingModule,
      ],
      providers: [
        WarehouseEffects,
        provideMockActions(() => actions$),
        { provide: ConfigurationService, useFactory: () => instance(configurationServiceMock) },
      ],
    });
    store$ = TestBed.inject(Store);
    effects = TestBed.inject(WarehouseEffects);
    warehouseServiceMock = mock(WarehouseService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
    expect(store$).toBeTruthy();
  });

  xdescribe('loadWarehouse$', () => {
    beforeEach(() => {
      when(warehouseServiceMock.getWarehouses()).thenReturn(of([mockedWarehouse]));
    });

    it('should call the warehouseService for loadWarehouses', done => {
      const action = loadWarehouses;
      actions$ = of(action);
      effects.loadWarehouse$.subscribe(() => {
        verify(warehouseServiceMock.getWarehouses()).once();
        done();
      });
    });

    it('should map to actions of type LoadWarehousesSuccess', () => {
      const action = loadWarehouses();
      const completion = loadWarehousesSuccess({
        warehouses: [],
      });
      actions$ = hot('-a-a-a', { a: action });
      const expected$ = cold('-c-c-c', { c: completion });

      expect(effects.loadWarehouse$).toBeObservable(expected$);
    });
  });
});
