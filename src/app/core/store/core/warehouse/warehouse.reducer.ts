import { createReducer, on } from '@ngrx/store';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';

import { loadWarehousesSuccess } from './warehouse.actions';

export interface WarehouseState {
  warehouses: Warehouse[];
  loading: boolean;
}

const initialState: WarehouseState = {
  loading: false,
  warehouses: [],
};

export const warehouseReducer = createReducer(
  initialState,
  on(loadWarehousesSuccess, (state, payload) => ({
    ...state,
    warehouses: payload.payload.warehouses,
  }))
);
