import { createSelector } from '@ngrx/store';

import { getCoreState } from 'ish-core/store/core/core-store';

const getWarehouseState = createSelector(getCoreState, state => state.warehouse);

export const getWarehouseLoading = createSelector(getWarehouseState, state => state.loading);

export const getWarehouses = createSelector(getWarehouseState, state => state.warehouses);
