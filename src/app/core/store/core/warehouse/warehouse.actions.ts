import { createAction } from '@ngrx/store';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';
import { payload } from 'ish-core/utils/ngrx-creators';

export const loadWarehouses = createAction('[Warehouse API] Load Warehouses');

export const loadWarehousesSuccess = createAction(
  '[Warehouse API] Load Warehouses successful',
  payload<{ warehouses: Warehouse[] }>()
);
