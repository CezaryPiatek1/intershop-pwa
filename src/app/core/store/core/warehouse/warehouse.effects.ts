import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';
import { WarehouseService } from 'ish-core/services/warehouse/warehouse.service';

import { loadWarehouses, loadWarehousesSuccess } from './warehouse.actions';

@Injectable()
export class WarehouseEffects {
  loadWarehouse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadWarehouses),
      switchMap(() =>
        this.warehouseService
          .getWarehouses()
          .pipe(map(warehouses => loadWarehousesSuccess({ warehouses: warehouses as Warehouse[] })))
      )
    )
  );
  constructor(private actions$: Actions, private warehouseService: WarehouseService) {}
}
