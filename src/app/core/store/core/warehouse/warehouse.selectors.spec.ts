import { TestBed } from '@angular/core/testing';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';
import { CoreStoreModule } from 'ish-core/store/core/core-store.module';
import { loadWarehousesSuccess } from 'ish-core/store/core/warehouse/warehouse.actions';
import { StoreWithSnapshots, provideStoreSnapshots } from 'ish-core/utils/dev/ngrx-testing';

import { getWarehouseLoading, getWarehouses } from './warehouse.selectors';

describe('Warehouse Selectors', () => {
  let store$: StoreWithSnapshots;
  const mockedWarehouse: Warehouse = { country: 'Japan', city: 'Tokyo', houseNumber: 3212 };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreStoreModule.forTesting(['warehouse'])],
      providers: [provideStoreSnapshots()],
    });

    store$ = TestBed.inject(StoreWithSnapshots);
  });

  describe('initial state', () => {
    it('should not be loading when in initial state', () => {
      expect(getWarehouseLoading(store$.state)).toBeFalse();
    });

    it('should be undefined or empty value for getWarehouse selector', () => {
      expect(getWarehouses(store$.state)).toBeEmpty();
    });
  });

  describe('after loaded data', () => {
    beforeEach(() => {
      store$.dispatch(loadWarehousesSuccess({ warehouses: [mockedWarehouse] }));
    });
    it('should have defined value for selector', () => {
      expect(getWarehouses(store$.state)).toStrictEqual([mockedWarehouse]);
    });
  });
});
