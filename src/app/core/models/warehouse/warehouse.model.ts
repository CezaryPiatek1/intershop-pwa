export interface Warehouse {
  country: string;
  city: string;
  houseNumber: number;
}
