import { TestBed } from '@angular/core/testing';
import { anything, instance, mock, verify } from 'ts-mockito';

import { ApiService } from 'ish-core/services/api/api.service';

import { WarehouseService } from './warehouse.service';

describe('Warehouse Service', () => {
  let apiServiceMock: ApiService;
  let warehouseService: WarehouseService;

  beforeEach(() => {
    apiServiceMock = mock(ApiService);
    TestBed.configureTestingModule({
      providers: [{ provide: ApiService, useFactory: () => instance(apiServiceMock) }],
    });
    warehouseService = TestBed.inject(WarehouseService);
  });

  it('should be created', () => {
    expect(warehouseService).toBeTruthy();
  });

  it('should always delegate to api service when called', () => {
    verify(apiServiceMock.get(anything())).never();
    warehouseService.getWarehouses();
    verify(apiServiceMock.get(anything())).once();
  });

  it("should call once 'warehouse' when 'getWarehouses' is called", () => {
    warehouseService.getWarehouses();
    verify(apiServiceMock.get('warehouse')).once();
  });
});
