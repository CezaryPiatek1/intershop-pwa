import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { WarehouseFacade } from 'ish-core/facades/warehouse.facade';
import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';

@Component({
  selector: 'ish-warehouse-page',
  templateUrl: './warehouse-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WarehousePageComponent implements OnInit {
  warehouseItems$: Observable<Warehouse[]>;

  constructor(private warehouseFacade: WarehouseFacade) {}

  ngOnInit() {
    this.warehouseItems$ = this.warehouseFacade.warehouses$;
    this.warehouseFacade.getWarehouses$();
  }
}
