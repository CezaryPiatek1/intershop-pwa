import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';

import { WarehouseListComponent } from './warehouse-list.component';

describe('Warehouse List Component', () => {
  let component: WarehouseListComponent;
  let fixture: ComponentFixture<WarehouseListComponent>;
  let element: HTMLElement;
  const mockedWarehouse: Warehouse = { country: 'Japan', city: 'Tokyo', houseNumber: 3212 };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WarehouseListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseListComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
    expect(element).toBeTruthy();
    expect(() => fixture.detectChanges()).not.toThrow();
  });

  it('should render h1 with title', () => {
    expect(element.querySelector('h1').textContent).toContain('OUR WAREHOUSES');
  });

  it('should render div with term on mock', () => {
    component.warehouseItems$ = new Observable(observer => {
      observer.next([mockedWarehouse]);
    });
    fixture.detectChanges();
    expect(element.querySelector('div').textContent).toContain('Japan, Tokyo, 3212');
  });

  it('shouldnt render div without data on warehouseItems$', () => {
    fixture.detectChanges();
    expect(element.querySelector('div')).toBeFalsy();
  });
});
