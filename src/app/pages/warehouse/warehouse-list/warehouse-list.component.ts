import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Warehouse } from 'ish-core/models/warehouse/warehouse.model';

@Component({
  selector: 'ish-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WarehouseListComponent {
  @Input() warehouseItems$: Observable<Warehouse[]>;
}
