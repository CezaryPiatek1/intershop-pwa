import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { instance, mock, when } from 'ts-mockito';

import { AppFacade } from 'ish-core/facades/app.facade';
import { WarehouseFacade } from 'ish-core/facades/warehouse.facade';

import { WarehouseListComponent } from './warehouse-list/warehouse-list.component';
import { WarehousePageComponent } from './warehouse-page.component';
import { of } from 'rxjs';
import { findAllCustomElements } from 'ish-core/utils/dev/html-query-utils';

describe('Warehouse Page Component', () => {
  let component: WarehousePageComponent;
  let fixture: ComponentFixture<WarehousePageComponent>;
  let element: HTMLElement;
  let warehouseFacade: WarehouseFacade;

  beforeEach(async () => {
    warehouseFacade = mock(WarehouseFacade);
    await TestBed.configureTestingModule({
      declarations: [MockComponent(WarehouseListComponent), WarehousePageComponent],
      providers: [
        { provide: AppFacade, useFactory: () => instance(mock(AppFacade)) },
        { provide: WarehouseFacade, useFactory: () => instance(warehouseFacade) },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehousePageComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
    expect(element).toBeTruthy();
    expect(() => fixture.detectChanges()).not.toThrow();
  });

  it('should render component if has empty array', () => {
    when(warehouseFacade.warehouses$).thenReturn(of([]));
    fixture.detectChanges();
    expect(findAllCustomElements(element)).toMatchInlineSnapshot(`
      Array [
        "ish-warehouse-list",
      ]
    `);
  });
});
