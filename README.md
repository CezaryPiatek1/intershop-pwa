# Intershop Progressive Web App

Welcome to the Intershop PWA project!

The Intershop PWA is an Angular-based progressive web app storefront for the Intershop Commerce Suite.

Accompany us on a journey for great cutting-edge eCommerce and take the chance to make it your journey, too.

If you want to get a first impression, please visit our [public demo](https://intershoppwa.azurewebsites.net/home).

More information on the PWA can be found [here](https://www.intershop.com/en/progressive-web-app).

In order to contribute, please have a look at our [Contribution Guidelines](./CONTRIBUTING.md)

---

## Getting Started

Head over to our documentation section for a [Quick Start Guide](./docs/guides/getting-started.md).

## License

The Intershop Progressive Web App is made available under the [MIT license](./LICENSE).

## PWA assignment
- Create new nav link in header name ‘Warehouses’
- Clicking on it will open a new page, which looks like the below screenshot
- Please create a mock API
- Use Intershop schematics to create a component, service, model, stores etc.
- Use Intershop conventions (folder structure, names etc.)
- Your assignment should have used facades, models, services, stores, page, component etc. 

# Screenshot of completed task:
![](https://i.imgur.com/nVKDUjY.png)
